import { Component, Input, OnInit } from '@angular/core';
import { ChatModalComponent } from '../chat-modal/chat-modal.component';
import { Router } from '@angular/router';
// import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss'],
})
export class ProfileCardComponent implements OnInit {
  constructor(private router: Router) {}
  @Input() profileData: any;
  ngOnInit(): void {}
  openConnectModal() {
    this.router.navigate(['/chat']);
  }
}
