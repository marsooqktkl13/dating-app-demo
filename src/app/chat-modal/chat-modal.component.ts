import { Component, OnInit } from '@angular/core';
import { ChatService, Message } from '../chat-service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-chat-modal',
  templateUrl: './chat-modal.component.html',
  styleUrls: ['./chat-modal.component.scss'],
})
export class ChatModalComponent implements OnInit {
  formGroup!: FormGroup;
  messages: Message[] = [];
  value!: string;

  constructor(
    public chatService: ChatService,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.chatService.conversation.subscribe((val) => {
      this.messages = this.messages.concat(val);
    });
  }
  sendMessage() {
    this.chatService.getBotAnswer(this.value);
    this.value = '';
  }
  clickToBack() {
    this.router.navigate(['']);
  }
  onSearchChange(e: any) {
    this.value = e.value;
  }
}
