import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageRoutingModule } from './page-routing.module';
import { PageComponent } from './page.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IonicModule } from '@ionic/angular';
import { ProfileCardComponent } from '../profile-card/profile-card.component';
import { HeaderComponent } from '../header/header.component';
import { ChatModalComponent } from '../chat-modal/chat-modal.component';
// import {  MatDialogModule } from '@angular/material/dialog';
import { ChatService } from '../chat-service';
import { LoginModule } from '../login/login.module';

@NgModule({
  declarations: [PageComponent, ProfileCardComponent, HeaderComponent],
  imports: [
    CommonModule,
    PageRoutingModule,
    FlexLayoutModule,
    IonicModule,
    LoginModule,
  ],
  providers: [ChatService],
  exports: [PageComponent],
})
export class PageModule {}
