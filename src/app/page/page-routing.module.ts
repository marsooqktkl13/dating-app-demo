import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageComponent } from './page.component';
import { ChatModalComponent } from '../chat-modal/chat-modal.component';

const routes: Routes = [
  { path: '', component: PageComponent },
  {
    path: 'chat',
    component: ChatModalComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageRoutingModule {}
