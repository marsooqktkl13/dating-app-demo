import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
})
export class PageComponent implements OnInit {
  currentCard = 0;
  recievedProfile: any;
  sentProfile: any;
  acceptedProfile: any;
  sortOption = [
    {
      id: 1,
      name: 'Sort By Age',
    },
    {
      id: 2,
      name: 'Sort By Name(A-Z)',
    },
    {
      id: 3,
      name: 'Sort By Name(Z-A)',
    },
  ];

  profileData = [
    {
      image: 'assets/brad.png',
      name: 'Erma Porter',
      age: '33',
      gender: 'Female',
      lookingFor: 'Man',
      location: 'Vern-sur-seiche Bretagne France',
      reqRec: false,
      reqSent: true,
      accepted: true,
    },
    {
      image: 'assets/bass.png',
      name: 'Brad Barber',
      age: '28',
      gender: 'Female',
      lookingFor: 'Man',
      location: 'Vern-sur-seiche Bretagne France',
      reqRec: true,
      reqSent: true,
      accepted: false,
    },
    {
      image: 'assets/kelly.png',
      name: 'Vicki Alvarez',
      age: '26',
      gender: 'Female',
      lookingFor: 'Man',
      location: 'Vern-sur-seiche Bretagne France',
      reqRec: true,
      reqSent: true,
      accepted: false,
    },
    {
      image: 'assets/opal.png',
      name: 'Amber Perry',
      age: '30',
      gender: 'Female',
      lookingFor: 'Man',
      location: 'Vern-sur-seiche Bretagne France',
      reqRec: true,
      reqSent: false,
      accepted: false,
    },
    {
      image: 'assets/viki.png',
      name: 'Kelly Fox',
      age: '24',
      gender: 'Female',
      lookingFor: 'Man',
      location: 'Vern-sur-seiche Bretagne France',
      reqRec: false,
      reqSent: false,
      accepted: false,
    },
    {
      image: 'assets/baily.png',
      name: 'Stewart Bailey',
      age: '30',
      gender: 'Male',
      lookingFor: 'Woman',
      location: 'Vern-sur-seiche Bretagne France',
      reqRec: true,
      reqSent: false,
      accepted: true,
    },
  ];
  headerData: any = [
    {
      name: 'Profiles',
    },
    {
      name: 'Request Recieved',
    },
    {
      name: 'Request Sent',
    },
    {
      name: 'Accepted Profiles',
    },
  ];
  activeTabClick(i: number) {
    this.currentCard = i++;
  }
  ngOnInit(): void {
    this.recievedProfile = this.profileData.filter((user) => {
      return user.reqRec === true;
    });
    this.sentProfile = this.profileData.filter((user) => {
      return user.reqSent === true;
    });
    this.acceptedProfile = this.profileData.filter((user) => {
      return user.accepted === true;
    });
  }
}
