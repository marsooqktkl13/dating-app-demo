import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  reqOtp = false;
  formGroup!: FormGroup;
  isSignup = false;
  otpSec = false;
  resendWarn = false;
  resendButton = false;
  counterStop: any;
  counter = 30;
  intervalId = 0;

  config = {
    allowNumbersOnly: false,
    length: 5,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '',
    inputStyles: {
      width: '40px',
      height: '40px',
    },
  };

  constructor(private fb: FormBuilder, private router: Router) {}
  gender = [
    {
      id: 1,
      name: 'Male',
    },
    {
      id: 2,
      name: 'Female',
    },
    {
      id: 3,
      name: 'Other',
    },
  ];

  Looking = [
    {
      id: 1,
      name: 'Male',
    },
    {
      id: 2,
      name: 'Female',
    },
    {
      id: 3,
      name: 'Both',
    },
  ];
  ngOnInit(): void {
    this.formGroup = this.fb.group({
      phoneNumber: ['', Validators.required],
      otp: [],
      firstName: [],
      lastName: [],
      age: [],
      gender: [],
      looking: [],
    });
  }
  requestOtp(phoneNumber: number, resend: any) {
    if (resend) {
      this.resendWarn = true;
    }
    this.reqOtp = true;
    this.otpSec = true;
    this.countDown();
  }
  signUpForm() {
    this.isSignup = true;
    this.reqOtp = true;
    this.otpSec = false;
  }
  verifyOtp() {
    this.router.navigate(['']);
  }
  submitForm() {
    this.isSignup = false;
    this.reqOtp = false;
  }
  countDown() {
    // this.clearTimer();
    this.intervalId = window.setInterval(() => {
      this.counter--;
      if (this.counter === -1) {
        this.resendButton = true;
      } else {
        if (this.counter <= 0) {
          this.counter = 0;
        } // reset
        this.counterStop = this.counter;
      }
    }, 1000);
  }
}
