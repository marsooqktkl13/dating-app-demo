import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginModule } from './login/login.module';
import { PageComponent } from './page/page.component';
import { PageModule } from './page/page.module';
import { ProfileCardComponent } from './profile-card/profile-card.component';
import { ChatModalComponent } from './chat-modal/chat-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
// import { MatDialog } from '@angular/material/dialog';

@NgModule({
  declarations: [AppComponent, ChatModalComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    IonicModule,
    PageModule,
    LoginModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
