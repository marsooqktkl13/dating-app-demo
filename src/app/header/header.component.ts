import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  whiteHeader!: boolean;
  mobileNavOpen = false;
  tooltipMenu!: boolean;
  isLogedIn!: boolean;
  constructor(private router: Router) {}
  items = {
    left: [
      { li: 'Community', link: 'store' },
      { li: 'About Us', link: 'custom-research' },
      { li: 'Contact Us', link: 'about-us' },
    ],
  };
  routeToHome() {
    this.router.navigate(['/']);
  }
  routeNav(link: any) {
    this.router.navigate(['/' + link]);
  }
}
